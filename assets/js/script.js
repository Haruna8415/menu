﻿$(function() {
  var $stick = $('.sp_menu');
  var $spOpen = $('.head_g');

  $stick.on('click',function(){
    $spOpen.toggleClass('head_g_open');
    });

    var headerHight = 100; //ヘッダの高さ
    $('a[href^=#]').click(function(){
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top-headerHight; //ヘッダの高さ分位置をずらす
        $("html, body").animate({scrollTop:position}, 550, "swing");
        return false;
    });
});
